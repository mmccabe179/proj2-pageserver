from flask import Flask, render_template, request, abort
import os

app = Flask(__name__)

@app.route("/<path:name>")
def index(name):
    fpath = "/templates/" + name
    if "//" in name or "~" in name or ".." in name:
	abort(403)
    if os.path.isfile("." + fpath):
	return render_template(name), 200		#Returning 200/Ok along with the html file contens
    else:
	if not os.path.isfile("." + fpath):
	    abort(404)

@app.errorhandler(404)					#Both 404.html and 403.html are in the templates folder
def error_404(n):
    return render_template("404.html"), 404		#Returning 404 error "File Not Found"

@app.errorhandler(403)
def error_403(n):
    return render_template("403.html"), 403 	#Returning 403 error "File Forbidden"


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
